import com.onresolve.scriptrunner.runner.rest.common.CustomEndpointDelegate
import groovy.json.JsonBuilder
import groovy.transform.BaseScript
import com.atlassian.applinks.api.ApplicationLink
import com.atlassian.jira.component.ComponentAccessor
import com.atlassian.jira.issue.CustomFieldManager
import com.atlassian.jira.issue.Issue
import com.atlassian.jira.issue.IssueManager
import com.atlassian.jira.config.properties.APKeys
import com.atlassian.jira.issue.search.SearchRequestEntity
import com.atlassian.jira.issue.search.SearchRequestManager
import com.atlassian.jira.issue.search.SearchProvider
import com.atlassian.jira.jql.parser.JqlQueryParser
import com.atlassian.jira.web.bean.PagerFilter
import com.atlassian.sal.api.component.ComponentLocator
import com.atlassian.sal.api.net.Request
import com.atlassian.sal.api.net.RequestFilePart
import com.atlassian.sal.api.net.Response
import com.atlassian.sal.api.net.ResponseException
import com.atlassian.sal.api.net.ResponseHandler
import com.atlassian.applinks.api.ApplicationLink
import com.atlassian.applinks.api.ApplicationLinkService
import com.atlassian.applinks.api.application.confluence.ConfluenceApplicationType
 
import javax.ws.rs.core.MultivaluedMap
import javax.ws.rs.core.Response

@BaseScript CustomEndpointDelegate delegate

generateColourSDONTree(httpMethod: "GET" 
                 ,groups: ["jira-core-users"]
                ) { MultivaluedMap queryParams, String body ->

    return Response.ok(returnSDONDocument(queryParams.get("filterID").first())).build();
}

 
def returnSDONDocument(filterID) {
    def jqlQueryParser = ComponentAccessor.getComponent(JqlQueryParser)
    def searchProvider = ComponentAccessor.getComponent(SearchProvider)
    def issueManager = ComponentAccessor.getIssueManager()
    def issueLinkManager = ComponentAccessor.getIssueLinkManager()
    def customFieldManager = ComponentAccessor.getCustomFieldManager()
    def user = ComponentAccessor.getJiraAuthenticationContext().getUser()
    def baseURL = ComponentAccessor.getApplicationProperties().getString(APKeys.JIRA_BASEURL).toString()
    def searchRequestManager = ComponentAccessor.getComponent(SearchRequestManager)    

    def epicColours = ['noEpic':'#CCCCCC','ghx-label-1':'#42526E','ghx-label-2':'#FFC400','ghx-label-3':'#FFE380','ghx-label-4':'#4C9AFF','ghx-label-5':'#00C7E6','ghx-label-6':'#79F2C0','ghx-label-7':'#C0B6F2','ghx-label-8':'#998DD9','ghx-label-9':'#FFBDAD','ghx-label-10':'#B3D4FF','ghx-label-11':'#79E2F2','ghx-label-12':'#EBECF0','ghx-label-13':'#57D9A3','ghx-label-14':'#FF8F73']
    def storyColours = ['noEpic':'#d7d7d7','ghx-label-1':'#8E96AA','ghx-label-2':'#FFE880','ghx-label-3':'#FFEEBB','ghx-label-4':'#94C2FF','ghx-label-5':'#9CEBF8','ghx-label-6':'#ADF6D8','ghx-label-7':'#DDDDFF','ghx-8':'#ADADE1','ghx-label-9':'#FFD2C5','ghx-label-10':'#CBE0FF','ghx-label-11':'#A0EBF5','ghx-label-12':'#F5F6FA','ghx-label-13':'#9BE9C7','ghx-label-14':'#FFC6B9']
    def subtaskHexColor = "#FFFFFF"
    
    def contentID = '20676644'
    def attachmentID = 'att21528607'
    
    //Setup Data tables
     // set the values for the Query data table label and fields
        def dataQueryTableID = 1
        def dataQueryTableName = "Data Table"
        // this maps the data table fields to the issue fields
        def dataQueryTableMap = ["QueryString":"QueryString"]
   // set the values for the main data table label and fields
      def dataTableID = 2
      def dataTableName = "Data Table"
      // this maps the data table fields to the issue fields
      def dataTableMap = ["Jira Key":"Key", "Issue Type":"Type", "Priority": "Priority", "Description":"Description","Assigned To":"Assignee", "Status":"Status", "Due Date": "DueDate", "Resolution":"Resolution"]

    def tableDef = ["Symbols":[{"Name":"Cloud","ID":"368d31f5-f374-48fe-8b29-11e1a43e4c8a"}],
                    "DataTable":[{"ID":"1","Name":"Details","Columns":[
                                {"Name":"Description","Type":"String"},
                                 {"Name":"Assigned To","Type":"String"},
                                {"Name":"Status","Type":"String"},
                                {"Name":"Due Date","Type":"Date"}],
                            {"ID":"2","Name":"Filter JQL","Columns":[
                                {"Name":"Request","Type":"String"}]
                                     }])
    //For a given project...PROJ
    def query = jqlQueryParser.parseQuery("filter in (${filterID}) ")
    log.debug(filterID)
    def filterDetails = searchRequestManager.getSearchRequestById(Long.valueOf(filterID))
    log.debug(filterDetails)
    
    //Pull list of results
    def results = searchProvider.search(query, user, PagerFilter.getUnlimitedFilter())

    //log.debug("Total issues: ${results.total}")

    def epics = [:]
    def stories = [:]
    def subtasks = [:]
    storyNoEpic = 'False'

    //Create the diagram map used to create the SDON document later
    def sdonMap = [:]

    //Set the diagram type you would want to create
    def diagramType = 'Tree'
    sdonMap.put('DiagramType', diagramType)

    //Create the Root shape
    def rootShapeMap= ['Label':filterDetails.name+'\n'+filterDetails.query.queryString,
    			'ShapeType':'RRect',
    			'FillColor':'#FFFFFF'
                'Data':{'TableID':'2','Fields':[{"QueryString":filterDetails.query.queryString}]}]

    //Create the map to hold the linked shapes from the root shape
    def defaultShape = ['ShapeType':'RRect']
    //Create an array to hold each shape
    def shapeListArray = []

    results.getIssues().each {documentIssue ->

        def issue = issueManager.getIssueObject(documentIssue.id)
        def links = issueLinkManager.getLinkCollection(documentIssue, user)
	//log.debug(documentIssue)
        switch (issue.getIssueType().getName()) {

            case 'Epic':
                //log.debug('Found Epic' + issue.getKey())
                epicColor = getEpicColourHex(documentIssue)
                //log.debug(epicColor)
                epics.put(issue.getKey(), ['summary' : documentIssue.summary ,'epicColor':epicColor])
                //log.debug('Epics: '+epics)
                break

            case 'Story':
                //log.debug('Found Story')
                def inLinks = links.getAllIssues() ?: []
                def epicLink = issue.getCustomFieldValue(customFieldManager.getCustomFieldObjectByName("Epic Link"))
                def epicKey
                if (epicLink) {
    //                //log.debug epicLink.key
                    epicKey = epicLink.key
                } else {
                epicKey = 'noEpic'
                storyNoEpic = 'True'
                log.debug(storyNoEpic)
                }
		//log.debug('EpicKey: '+epicKey)
                stories.put(issue.getKey(), ['summary' : documentIssue.summary, 'parent' : epicKey])
                break
            case 'Sub-task':
                def parentKey = issue.getParentObject().toString()
                subtasks.put(issue.getKey(), ['summary' : documentIssue.summary, 'parent' : parentKey])

                break
        }

    }
    //log.debug(epics)
    if (storyNoEpic == 'True') {
    epics.put('noEpic',['summary':'None', 'epicColor':'noEpic'])
    }
    //log.debug(epics)
    
    epics.each(  { epic, epicMap ->
        def shapeEntry = [:]
	
        epicHexColor = epicColours.getAt(epicMap.epicColor)
        storyHexColor = storyColours.getAt(epicMap.epicColor)
        log.debug "Epic: ${epic}  - ${epicMap.summary} [${epicHexColor} - ${storyHexColor} - ${subtaskHexColor}]"
        def childShapeArray = []
        //Get all stories
        def childStories = stories.findAll({story, storyMap ->
            storyMap.parent == epic
        })

        childStories.each( { story, storyMap ->
            if (storyMap.parent == epic) {
                def childShapeMap = [:]
                childShapeMap.put('Label', "Story - ${storyMap.summary}")
                childShapeMap.put("Hyperlink", ["url":"${baseURL}/browse/${story}"])
                childShapeMap.put('FillColor', storyHexColor)
                childShapeMap.put('LineColor', epicHexColor)
                //log.debug "\tStory: ${childShapeMap}"
                childShapeMap.put('ID', "${story}")
                childShapeArray.push(childShapeMap)
                //log.debug "Current childShapeArray: ${childShapeArray}"
                def childShapeListArray = []
                subtasks.each({ subtask, subtaskMap ->
                    if (subtaskMap.parent == story) {
                        def childShapeListMap = [:]
                        childShapeListMap.put('Label', "Subtask - ${subtaskMap.summary}")
                        childShapeListMap.put("Hyperlink", ["url":"${baseURL}/browse/${subtask}"])
                        childShapeListMap.put('FillColor', subtaskHexColor)
                        childShapeListMap.put('LineColor', storyHexColor)
                        childShapeListMap.put('ID', "${subtask}")
                        //log.debug "\tSubtask: ${childShapeListMap}"
                        childShapeListArray.add(childShapeListMap)
                    }
                })
                childShapeMap.put('ShapeConnector' , [[ 'Shapes' : childShapeListArray]])

            }


        })
        shapeEntry.put('Label' , "Epic - ${epicMap.summary}")
        shapeEntry.put("Hyperlink", ["url":"${baseURL}/browse/${epic}"])
        shapeEntry.put('FillColor', epicHexColor)
        shapeEntry.put('ShapeConnector' , [['Arrangement':'Column', 'Shapes' : childShapeArray]])

        shapeListArray.push(shapeEntry)
        //log.debug "Current shapeListArray: ${shapeListArray}"
    })
    //log.debug "Current shapeListArray: ${shapeListArray}"

    //Add the shapes list to the
    rootShapeMap.put('ShapeConnector', [['DefaultShape':defaultShape,'Shapes' : shapeListArray]])

    sdonMap.put('RootShape', [rootShapeMap])

    def sdonDocument = new JsonBuilder(sdonMap).toString()
    return sdonDocument
}

def getEpicColourHex(def issue){
    CustomFieldManager customFieldManager = ComponentAccessor.getCustomFieldManager()
    def epicColour = issue?.getCustomFieldValue(customFieldManager.getCustomFieldObjectsByName("Epic Color")?.first())
    return epicColour
}
